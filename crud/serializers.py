from django.utils import timezone
from rest_framework import serializers
from crud.models import CustomUser


class DynamicSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields', None)
        super(DynamicSerializer, self).__init__(*args, **kwargs)
        if fields is not None:
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class UserSerializer(DynamicSerializer):
    new_password = serializers.CharField(required=True)

    class Meta:
        model = CustomUser
        fields = (
        'id', 'first_name', 'last_name', 'username', 'email', 'password', 'modified_date', 'date_joined',
        'is_active', 'new_password', 'user_created_id', 'user_updated_id')

    def validate_username(self, username):
        if len(username) > 16 or len(username) < 6:
            raise serializers.ValidationError('El usuario debe contener entre 6 y 16 caracteres')
        return username

    def create(self, validated_data):
        return CustomUser.objects.create_user(**validated_data)


class UsersPassword(DynamicSerializer):
    repeat_password = serializers.CharField(required=True)

    class Meta:
        model = CustomUser
        fields = ('password', 'repeat_password')

    def validate(self, data):
        if data['password'] != data['repeat_password']:
            raise serializers.ValidationError("Las contrasenas no coinciden")
        return data


class UsersMinSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'is_active')
