from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from crud.views import UserList, UserDetail

urlpatterns = format_suffix_patterns([
    path('users/', UserList.as_view({'get': 'list', 'post': 'create'}), name='user-list'),
    path('users/<int:pk>/', UserDetail.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}),
         name='user-detail'),
    path('users/<int:pk>/status/', UserDetail.as_view({'put': 'status'}), name='user-status'),
    path('users/<int:pk>/change_password/', UserDetail.as_view({'put': 'change_password'}),
         name='user-change-password')
])

