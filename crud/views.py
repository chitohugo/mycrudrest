from django.utils import timezone
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets
from crud.models import CustomUser
from crud.serializers import UserSerializer, UsersPassword
from myproject.common import Users


################## LIST Y CREATE USER ######################


class UserList(viewsets.ModelViewSet):
    queryset = CustomUser.objects.filter(is_active__gt=0).order_by('date_joined')
    serializer_class = UserSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, fields=(
            'id', 'first_name', 'last_name', 'username', 'email', 'is_active', 'date_joined'), many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data,
                                         fields=('id', 'first_name', 'last_name', 'username', 'email', 'password'))
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer, request)
        return Response(serializer.data['id'], status=status.HTTP_201_CREATED)

    def perform_create(self, serializer, request):
        serializer.save(user_created_id=request.user,
                        user_updated_id=request.user)


################## DELETE AND UPDATE OF USER ######################
class UserDetail(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = None

    def retrieve(self, request, pk):
        instance = Users.get_object(pk)
        queryset = CustomUser.objects.get(id=instance.id)
        serializer = self.get_serializer(queryset, fields=(
            'id', 'first_name', 'last_name', 'username', 'email', 'date_joined', 'is_active'), many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, **kwargs)

    def update(self, request, id, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = Users.get_object(id)
        serializer = self.get_serializer(instance, data=request.data,
                                         fields=('first_name', 'last_name', 'username', 'email', 'admin_updated_id'),
                                         partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer, request)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    def perform_update(self, serializer, request):
        serializer.save(admin_updated_id=request.user, modified_date=timezone.now())

    def destroy(self, request, id):
        Users.get_object(id)
        CustomUser.objects.filter(id=id).update(
            is_active=0,
            admin_updated_id=request.user,
            modified_date=timezone.now())
        return Response(status=status.HTTP_202_ACCEPTED)

    def status(self, request, id):
        Users.get_object(id)
        CustomUser.objects.filter(id=id).update(is_active=request.data['status'],
                                                user_updated_id=request.user,
                                                modified_date=timezone.now())
        return Response(status=status.HTTP_202_ACCEPTED)

    def change_password(self, request, id):
        instance = Users.get_object(id)
        serializer = self.get_serializer(data=request.data, fields=('password', 'new_password'))
        if serializer.is_valid():
            # Check old password
            if instance and instance.check_password(serializer.data['password']):
                instance.set_password(serializer.data.get('new_password'))
                instance.save()
                return Response(status=status.HTTP_202_ACCEPTED)
            return Response({'password': ['Contresena Incorrecta']}, status=status.HTTP_400_BAD_REQUEST)
