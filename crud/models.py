from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.

class CustomUser(AbstractUser):
    id = models.AutoField(primary_key=True)
    modified_date = models.DateTimeField(null=True, blank=True)
    email = models.CharField(max_length=150, unique=True)
    is_active = models.IntegerField(default=1, null=True, blank=True)
    user_created_id = models.ForeignKey('self', null=True, on_delete=models.CASCADE, related_name='admin_created_id')
    user_updated_id = models.ForeignKey('self', null=True, on_delete=models.CASCADE, related_name='admin_update_id')

    class Meta:
        db_table = 'custouser'

    def __unicode__(self):
        return unicode(self.id)

    def __str__(self):
        return 'User: {} {}'.format(self.id, self.username)
