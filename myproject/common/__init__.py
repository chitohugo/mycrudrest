from django.http import Http404
from crud.models import CustomUser


class Users(object):
    def get_object(pk):
        try:
            return CustomUser.objects.get(id=pk, is_active__gte=1)
        except CustomUser.DoesNotExist:
            raise Http404

    def get_object_active(pk):
        try:
            return CustomUser.objects.get(id=pk, is_active=1)
        except CustomUser.DoesNotExist:
            raise Http404

    def get_object_username(username):
        try:
            return CustomUser.objects.get(username=username, is_active=1)
        except CustomUser.DoesNotExist:
            raise Http404

    def get_object_email(email):
        try:
            return CustomUser.objects.get(email=email, is_active=1)
        except CustomUser.DoesNotExist:
            raise Http404
